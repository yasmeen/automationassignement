package e_commerce.pages;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductListingPage {

	//page objects
	private WebDriver driver;
	
	public ProductListingPage ()
	{
		this.driver=driver;
		
		PageFactory.initElements(driver,this);
		
	}
	
	
	public void setProductName(String Prname)
	{
		
	}
	
	public void setProductDescription(String desc)
	{
		
	}
	public void setPrice(double pr)
	{
		
	}
	public void uploadPicture()
	{
		
	}
	public void AddItem()
	{
		
	}
	
	public ProductDetailPage ClickProductFromList(WebElement prod)
	{
		click on prod
		new WebDriverWait(driver,20).until(ExpectedConditions.invisibilityOf("PLP object"));
		return new ProductDetailPage(driver);
		
	}
	
	public void deleteProduct(String Product)
	{
		
	}
	
	public void FetchAllProductsfromList() {
		
	}
	
	public boolean verifyProductInDatabase(String product)
	{
		String dbUrl="jdbc:mysql://localhost:3036/products";
		String userName="user1",pwd="1234",query="Select * from products where productName=";
		query=query+product+";";
		try {
			Class.forName("com.mysql.jdbc.driver");
			Connection con=DriverManager.getConnection(dbUrl,userName,pwd);
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(query);
			
			while(rs.next())
			{
				if(rs.getString(2).equalsIgnoreCase(product))
				{
					return true;
										
				}
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
	
	
}
