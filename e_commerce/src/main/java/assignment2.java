import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class assignment2 {
	
	static By GetStartedButton=By.xpath("//div/button[@type='submit']");
	static By NameTextBox=By.id("name");
	static By OrgTextBox=By.id("orgName");
	static By emailTextBox=By.id("singUpEmail");
	static By IAgreeChkBox=By.xpath("//span[text()='I agree to the']");
	static By successMsg=By.xpath("//*[contains(text(),'A welcome email has been sent')]");
	static By dropdown=By.xpath("(//span[@class='ui-select-match-text pull-left'])[1]");

	static List<String> ExpectedLang;
	static WebDriver driver;
	static String  browser="Chrome",myName="yasmeen sayyed",email="yasmeen.sayyed@gmail.com";
	public static void main(String args[])
	{
		if(browser.equalsIgnoreCase("chrome"))
				{
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + " path to chromedriver_91.exe");
			driver=new ChromeDriver();

		}
		else if(browser.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver","path to driver");
			driver=new FirefoxDriver();
		}
		
		driver.get("http://jt-dev.azurewebsites.net/#/SignUp");
		driver.manage().window().maximize();
		WebElement dropdown1=driver.findElement(dropdown);
		ExpectedLang.add("English");
		ExpectedLang.add("Detuch");
		
		Select lang=new Select(dropdown1);
		List <String> arr1=new ArrayList<String>();
			List<WebElement>options = lang.getOptions();
			
			for(WebElement ele: options)
			arr1.add(ele.getText());
			
			Collections.sort(arr1);
			Collections.sort(ExpectedLang);
			Assert.assertTrue(arr1.equals(ExpectedLang));
			
			lang.selectByValue("English");
		
			
			driver.findElement(NameTextBox).sendKeys(myName);
		driver.findElement(OrgTextBox).sendKeys(myName);
		driver.findElement(emailTextBox).sendKeys(email);
		driver.findElement(IAgreeChkBox).click();
		
		new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(GetStartedButton));
		
		driver.findElement(GetStartedButton).click();
		Assert.assertTrue(driver.findElement(successMsg).isDisplayed());

	}

}
