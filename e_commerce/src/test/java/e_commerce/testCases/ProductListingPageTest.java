package e_commerce.testCases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import e_commerce.pages.ProductDetailPage;
import e_commerce.pages.ProductListingPage;

public class ProductListingPageTest {
	
	ProductListingPage PLP;
	WebDriver driver;
	double p=23.56;
	String prodName="pr1",desc="desc";
	ProductDetailPage PDP;
	
	@Test
	public void AddItem()
	{
		
		PLP=new ProductListingPage();
		PLP.setProductName(prodName);
		PLP.setProductDescription(desc);
		PLP.setPrice(p);
		PLP.uploadPicture();
		PLP.AddItem();
	}
	
	@Test
	public void ValidateProductinDB()
	{
		PLP=new ProductListingPage();
		PLP.setProductName(prodName);
		PLP.setProductDescription(desc);
		PLP.setPrice(p);
		PLP.uploadPicture();
		PLP.AddItem();
		Assert.assertTrue(PLP.verifyProductInDatabase(prodName));
		
	}
	@Test
	public void OpenPDP()
	{
		PLP=new ProductListingPage();
		ProductDetailPage PDP =PLP.ClickProductFromList();
		Assert.assertTrue(PDP.headerIsDisplayed());
	}
	
	@Test
	public deleteProduct()
	{
		PLP=new ProductListingPage();
		PLP.setProductName(prodName);
		PLP.setProductDescription(desc);
		PLP.setPrice(p);
		PLP.uploadPicture();
		PLP.AddItem();
		PLP.deleteProduct(prodName);
	}
	
		

}
