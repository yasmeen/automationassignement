package e_commerce.testCases;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import utils.ResusableUtils;

public class BaseTest {
	
	private static WebDriver driver;
	ResusableUtils util1;
	@BeforeTest
	public void SetUp()
	{
		System.setProperty("webdriver.chrome.driver", "pathToChromedriver.exe");
		driver=new ChromeDriver();
		Properties prop=util1.readProperties();
		driver.get(prop.getProperty("APP_URL"));
		
		
	}

	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}
	
	public WebDriver getDriver()
	{
		return driver;
	}
}
